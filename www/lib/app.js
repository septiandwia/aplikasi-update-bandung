// angular.module('my-app', ['onsen'])

var ROOT_URL = "http://hitamcoklat.com/updatebandung";
// var ROOT_URL = "http://localhost/mobile_project/aplikasi_ulinkeun/api";

ons.bootstrap()

.filter('unsafe', function($sce) {

    return function(val) {
        return $sce.trustAsHtml(val);
    };

})

.filter('unsafeHome', function($sce) {

    return function(val) {
        var str = val.substring(0, 100);
        str = str + ' ...';
        return $sce.trustAsHtml(str);
    };

})

.controller('home', function($scope, $http, $timeout) {

    ons.createPopover('popover.html').then(function(popover) {
        $scope.popover = popover;
    });

    $scope.options = ['Refresh', 'About'];

    function randomWarna() {
        // RANDOM BACKGROUND COLOR
        var warna = ["#085078", "#373B44", "#D38312", "#660000", "#71B4B7", "#5D4C46", "#9F6164", "#06425C", "#6F3662", "#CDBB79"];
        warna = warna[Math.floor(Math.random() * warna.length)];

        return warna;
    }

    $scope.loadingBar = true;

    $http.get(ROOT_URL + '/article')
        .then(function successCallback(response) {
                $scope.loadingBar = false;
                $scope.warnaBackground = randomWarna();
                $scope.id = response.data.id;
                $scope.ringkasan = response.data.ringkasan;
                $scope.judul = response.data.judul;
                $scope.items = response.data.konten;
                // console.log($scope.items);
            },
            function errorCallback(response) {
                console.log("gagal");
            });

    $scope.clickAbout = function() {
        $scope.popover.hide();
        console.log('about');
        myNavigator.pushPage('about', { animation: 'slide' });
    }

    $scope.perKategori = function(jenis) {
        $scope.toJenis = jenis;
        $scope.loadingBar = true;
        $scope.contentImageWrap = true;
        console.log('Kategori ' + jenis + ' di Click!');

        if (jenis == 'article') {
            jenis = '/json/artikel/list.json';
            $scope.titleHead = "Arsip";
        } else {
            jenis = '/json/event/list.json';
            $scope.titleHead = "Event, Loker dan lainnya";
        }

        $http.get(ROOT_URL + jenis)
            .then(function successCallback(response) {
                    $scope.contentImageWrap = false;
                    $scope.loadingBar = false;
                    $scope.itemPerKategori = response.data;
                    myNavigator.pushPage('perKategori', { animation: 'slide' });
                },
                function errorCallback(response) {
                    console.log("gagal");
                });

    }


    $scope.getData = function(id) {

        window.cache.clear();
        window.cache.cleartemp();

        $scope.popover.hide();
        $scope.loadingBar = true;
        $scope.contentImageWrap = true;

        $http.get(ROOT_URL + '/article?except=' + id)
            .then(function successCallback(response) {

                    $scope.contentImageWrap = false;
                    $scope.loadingBar = false;
                    $scope.warnaBackground = randomWarna();
                    $scope.id = response.data.id;
                    $scope.ringkasan = response.data.ringkasan;
                    $scope.judul = response.data.judul;
                    $scope.items = response.data.konten;
                    // $scope.refresh();
                },
                function errorCallback(response) {
                    console.log("gagal");
                });


    };

    $scope.lihatMap = function(longlat, judul) {
        cordova.InAppBrowser.open('http://hitamcoklat.com/map/?w=' + window.innerWidth + '&h=' + window.innerHeight + '&longlat=' + longlat + '&t=' + judul, '_blank', 'location=no');
    }

    $scope.tulisInfo = function() {
        cordova.InAppBrowser.open('http://hitamcoklat.com/updatebandung/event/add', '_self', 'location=no');
    }

    $scope.showDetail = function(id, idDetail) {

        $scope.loadingBar = true;

        $http.get(ROOT_URL + '/json/artikel/' + id + '.json')
            .then(function successCallback(response) {

                    $scope.loadingBar = false;

                    var data = JSON.parse(response.data.read[0].konten);
                    var details = {
                        "data": [
                            { konten: data.konten[idDetail], image: data.images[idDetail], imageDes: data.image_des[idDetail], judul: data.subjudul[idDetail], imageSource: data.image_source[idDetail] },
                        ]
                    };
                    myNavigator.pushPage('detail', { animation: 'fade' });
                    $scope.data = details.data[0];
                    // console.log($scope.data);

                },
                function errorCallback(response) {
                    console.log(response);

                });
    }

    $scope.shareInfo = function(judul, deskripsi, id, image, link) {

        window.plugins.socialsharing.share(judul + ' --> ' + link, judul, image, null);

    }

    // SHOW DETAIL EVENT
    $scope.showDetaiKategori = function(id, jenis) {

        if (jenis == 'event') path = '/json/event/';
        else path = '/json/artikel/';

        $scope.loadingBar = true;
        $scope.contentImageWrap = true;

        $http.get(ROOT_URL + path + id + '.json')
            .then(function successCallback(response) {
                    $scope.contentImageWrap = false;
                    $scope.loadingBar = false;

                    if (jenis != 'event') {
                        // console.log(response);
                        $scope.warnaBackground = randomWarna();
                        $scope.perKategoriid = response.data.read[0].id;
                        $scope.perKategoriRingkasan = response.data.read[0].ringkasan;
                        $scope.perKategorijudul = response.data.read[0].judul;
                        $scope.datas = JSON.parse(response.data.read[0].konten);
                        // $scope.datas = {
                        //     "data": [
                        //         { konten: data.konten[idDetail], image: data.images[idDetail], imageDes: data.image_des[idDetail], judul: data.subjudul[idDetail], imageSource: data.image_source[idDetail] },
                        //     ]
                        // };
                    } else {
                        // KALO HALAMAN DETAIL INFO MASUK KESINI
                        console.log(response.data.read[0].shortUrl);
                        response.data.read[0].file_image = ROOT_URL + '/upload' + response.data.read[0].file_image;
                        $scope.shorturl = response.data.read[0].shortUrl;
                        $scope.datas = response.data.read[0];
                        // console.log($scope.datas);
                    }

                    if (jenis == 'event') myNavigator.pushPage('detailEvent', { animation: 'fade' });
                    else myNavigator.pushPage('detailArsip', { animation: 'fade' });

                },
                function errorCallback(response) {
                    console.log("gagal");
                });

    }

})
